v {xschem version=3.1.0 file_version=1.2
}
G {}
K {}
V {}
S {}
E {}
N 90 -170 90 -150 {
lab=vdd}
N 90 -90 90 -70 {
lab=GND}
N 330 -260 330 -210 {
lab=vdd}
N 330 -70 330 -50 {
lab=GND}
N 430 -140 450 -140 {
lab=out}
N 290 -150 300 -150 {
lab=A}
N 330 -80 330 -70 {
lab=GND}
N 420 -140 430 -140 {
lab=out}
N 330 -210 330 -200 {
lab=vdd}
N 360 -260 360 -200 {
lab=cnt}
N 270 -120 290 -120 {
lab=B}
N 290 -120 300 -120 {
lab=B}
N 450 -50 450 -30 {
lab=GND}
N 250 -150 270 -150 {
lab=A}
N 280 -150 290 -150 {
lab=A}
N 450 -140 450 -110 {
lab=out}
N 270 -150 280 -150 {
lab=A}
N 250 -120 270 -120 {
lab=B}
C {devices/vsource.sym} 90 -120 0 0 {name=V1 value=3}
C {devices/gnd.sym} 90 -70 0 0 {name=l1 lab=GND}
C {devices/lab_pin.sym} 90 -170 0 0 {name=l2 sig_type=std_logic lab=vdd}
C {devices/lab_pin.sym} 330 -260 0 0 {name=l3 sig_type=std_logic lab=vdd}
C {devices/gnd.sym} 330 -50 0 0 {name=l4 lab=GND}
C {devices/lab_pin.sym} 250 -150 0 0 {name=l7 sig_type=std_logic lab=A}
C {devices/code_shown.sym} 10 30 0 0 {name=s1 only_toplevel=true value="

.lib /foss/pdks/sky130A/libs.tech/ngspice/sky130.lib.spice tt

va A 0 AC=1
vb B 0 dc=0
ven cnt 0 dc=0

.control
save all
ac dec 200 10 1G
settype decibel out
plot vdb(out)
.endc

.control
alter va AC=0
alter vb AC=1
alter ven dc=3
save all
ac dec 200 10 1G
settype decibel out
plot vdb(out)
wrdata /foss/designs/mux_data_crosstalk.dat vdb(out)
.endc

.control
save all
save @m.xmux_2to1_1.xtgate1.xxp0.xm1.msky130_fd_pr__pfet_01v8_lvt[id]
save @m.xmux_2to1_1.xtgate1.xxn0.xm1.msky130_fd_pr__nfet_01v8_lvt[id]
dc va 3.3 0 -0.001
let idspmos = @m.xmux_2to1_1.xtgate1.xxp0.xm1.msky130_fd_pr__pfet_01v8_lvt[id]
let idsnmos = @m.xmux_2to1_1.xtgate1.xxn0.xm1.msky130_fd_pr__nfet_01v8_lvt[id]
wrdata /foss/designs/mux_data.dat v(a) idsnmos idspmos
.endc

"}
C {mux_2to1_gen/mux_2to1_templates_xschem/mux_2to1/mux_2to1.sym} 270 -70 0 0 {name=mux_2to1_1
spiceprefix=X
}
C {devices/lab_pin.sym} 360 -260 0 0 {name=l11 sig_type=std_logic lab=cnt}
C {devices/lab_pin.sym} 250 -120 0 0 {name=l14 sig_type=std_logic lab=B}
C {devices/res.sym} 450 -80 0 0 {name=R1
value=1k
footprint=1206
device=resistor
m=1}
C {devices/gnd.sym} 450 -30 0 0 {name=l15 lab=GND}
C {devices/lab_pin.sym} 450 -140 2 0 {name=l8 sig_type=std_logic lab=out}
