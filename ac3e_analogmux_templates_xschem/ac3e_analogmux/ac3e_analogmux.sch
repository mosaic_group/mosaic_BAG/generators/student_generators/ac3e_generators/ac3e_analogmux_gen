v {xschem version=3.4.4 file_version=1.2
}
G {}
K {}
V {}
S {}
E {}
N 140 -270 160 -270 {
lab=CNT}
N 510 -245 510 -195 {
lab=EN}
N 520 -245 520 -225 {
lab=VSS}
N 520 -155 520 -135 {
lab=VDD}
N 520 -75 520 -50 {
lab=VSS}
N 510 -75 510 -35 {
lab=ENB}
N 465 -105 480 -105 {
lab=IN}
N 540 -275 570 -275 {
lab=OUTA}
N 540 -105 570 -105 {
lab=OUTB}
N 465 -275 480 -275 {
lab=IN}
N 510 -335 510 -305 {
lab=ENB}
N 520 -325 520 -305 {
lab=VDD}
N 510 -345 510 -335 {
lab=ENB}
N 510 -35 510 -30 {
lab=ENB}
N 520 -345 520 -325 {
lab=VDD}
N 510 -365 510 -345 {
lab=ENB}
N 510 -195 510 -135 {
lab=EN}
N 520 -175 520 -155 {
lab=VDD}
N 520 -225 520 -205 {
lab=VSS}
N 520 -205 520 -195 {
lab=VSS}
N 440 -275 465 -275 {
lab=IN}
N 455 -275 455 -105 {
lab=IN}
N 455 -105 465 -105 {
lab=IN}
N 220 -270 270 -270 {
lab=ENB}
N 190 -330 190 -290 {
lab=VDD}
N 190 -250 190 -210 {
lab=VSS}
N 300 -250 300 -210 {
lab=#net1}
N 300 -330 300 -290 {
lab=VDD}
N 330 -270 340 -270 {}
C {devices/iopin.sym} 60 -290 2 0 {name=p1 lab=CNT}
C {devices/iopin.sym} 60 -270 2 0 {name=p2 lab=IN}
C {devices/iopin.sym} 60 -250 2 0 {name=p3 lab=OUTA}
C {devices/iopin.sym} 60 -230 2 0 {name=p4 lab=OUTB}
C {devices/iopin.sym} 60 -210 2 0 {name=p5 lab=VDD}
C {devices/iopin.sym} 60 -190 2 0 {name=p6 lab=VSS}
C {inverter2_gen/inverter2_templates_xschem/inverter2/inverter2.sym} 170 -250 0 0 {name=inv1
}
C {inverter2_gen/inverter2_templates_xschem/inverter2/inverter2.sym} 280 -250 0 0 {name=inv2
}
C {tgate_gen/tgate_templates_xschem/tgate/tgate.sym} 470 -235 0 0 {name=tgate1}
C {tgate_gen/tgate_templates_xschem/tgate/tgate.sym} 470 -65 0 0 {name=tgate2
}
C {devices/lab_pin.sym} 235 -270 3 0 {name=l1 sig_type=std_logic lab=ENB}
C {devices/lab_pin.sym} 340 -270 2 0 {name=l2 sig_type=std_logic lab=EN}
C {devices/lab_pin.sym} 140 -270 0 0 {name=l4 sig_type=std_logic lab=CNT}
C {devices/lab_pin.sym} 520 -175 2 0 {name=l10 sig_type=std_logic lab=VDD}
C {devices/lab_pin.sym} 520 -195 2 0 {name=l11 sig_type=std_logic lab=VSS}
C {devices/lab_pin.sym} 520 -345 2 0 {name=l12 sig_type=std_logic lab=VDD}
C {devices/lab_pin.sym} 520 -50 2 0 {name=l13 sig_type=std_logic lab=VSS}
C {devices/lab_pin.sym} 510 -365 2 0 {name=l14 sig_type=std_logic lab=ENB}
C {devices/lab_pin.sym} 510 -30 2 0 {name=l15 sig_type=std_logic lab=ENB}
C {devices/lab_pin.sym} 510 -190 0 0 {name=l16 sig_type=std_logic lab=EN}
C {devices/iopin.sym} 60 -160 2 0 {name=p7 lab=EN}
C {devices/iopin.sym} 60 -140 2 0 {name=p8 lab=ENB}
C {devices/lab_pin.sym} 570 -275 2 0 {name=l8 sig_type=std_logic lab=OUTA}
C {devices/lab_pin.sym} 570 -105 2 0 {name=l9 sig_type=std_logic lab=OUTB}
C {devices/lab_pin.sym} 440 -275 0 0 {name=l17 sig_type=std_logic lab=IN}
C {devices/lab_pin.sym} 190 -330 2 0 {name=p9 sig_type=std_logic lab=VDD}
C {devices/lab_pin.sym} 190 -210 2 0 {name=p10 sig_type=std_logic lab=VSS}
C {devices/lab_pin.sym} 300 -330 2 0 {name=p11 sig_type=std_logic lab=VDD}
C {devices/lab_pin.sym} 300 -210 2 0 {name=p12 sig_type=std_logic lab=VSS}
