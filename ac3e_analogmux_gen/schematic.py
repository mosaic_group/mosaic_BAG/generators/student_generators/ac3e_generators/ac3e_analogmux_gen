import os
from typing import *
from bag.design import Module

yaml_file = os.path.join(f'{os.environ["BAG_GENERATOR_ROOT"]}/BagModules/ac3e_analogmux_templates',
                         'netlist_info', 'ac3e_analogmux.yaml')


# noinspection PyPep8Naming
class schematic(Module):
    """Module for library ac3e_analogmux_templates cell ac3e_analogmux.

    Fill in high level description here.
    """

    def __init__(self, bag_config, parent=None, prj=None, **kwargs):
        super().__init__(bag_config, yaml_file, parent=parent, prj=prj, **kwargs)
       
    @classmethod
    def get_params_info(cls) -> Dict[str, str]:
        """Returns a dictionary from parameter names to descriptions.

        Returns
        -------
        param_info : Optional[Dict[str, str]]
            dictionary from parameter names to descriptions.
        """
        return dict(
            inv_params='Inverter schematic parameters dictionary',
            tgate_params='Tgate schematic parameters dictionary',
            )

    def design(self,
               inv_params: Dict[str, Any],
               tgate_params: Dict[str, Any]):
        """To be overridden by subclasses to design this module.

        This method should fill in values for all parameters in
        self.parameters.  To design instances of this module, you can
        call their design() method or any other ways you coded.

        To modify schematic structure, call:

        rename_pin()
        delete_instance()
        replace_instance_master()
        reconnect_instance_terminal()
        restore_instance()
        array_instance()
        """

        self.instances['inv1'].design(**inv_params)
        self.instances['inv2'].design(**inv_params)
        self.instances['tgate1'].design(**tgate_params)
        self.instances['tgate2'].design(**tgate_params)
