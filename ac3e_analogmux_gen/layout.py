from typing import *

from bag.layout.routing import TrackID
from bag.layout.template import TemplateBase

from sal.layout.manual_router import ManualRouter, ManualRouting, UPPER, LOWER, TrackConnection, TrackIntersectionPoint
from sal.layout.placement import PlacedInstances

from inverter2_gen.layout import inverter2
from tgate_gen.layout import tgate
from .params import ac3e_analogmux_layout_params


class layout(TemplateBase):
    """A 2 to 1 Multiplexer including inverter and tgate sub-blocks.

    Parameters
    ----------
    temp_db : :class:`bag.layout.template.TemplateDB`
           the template database.
    lib_name : str
       the layout library name.
    params : dict[str, any]
       the parameter values.
    used_names : set[str]
       a set of already used cell names.
    **kwargs :
       dictionary of optional parameters.  See documentation of
       :class:`bag.layout.template.TemplateBase` for details.
    """
    def __init__(self, temp_db, lib_name, params, used_names, **kwargs):
        super().__init__(temp_db, lib_name, params, used_names, **kwargs)
        self._sch_params = None
    
    @classmethod
    def get_params_info(cls) -> Dict[str, str]:
        """
        Returns a dictionary containing parameter descriptions.

        Override this method to return a dictionary from parameter names to descriptions.

        Returns
        -------
        param_info : dict[str, str]
           dictionary from parameter name to description.
        """
        return dict(
            params='ac3e_analogmux_layout_params parameters object',
        )

    def draw_layout(self):
        """Draw the layout."""

        # make copies of given dictionaries to avoid modifying external data.
        params: ac3e_analogmux_layout_params = self.params['params'].copy()
        inv_params = params.inverter_params
        tgate_params = params.tgate_params

        # disable pins in subcells
        inv_params.show_pins = False
        tgate_params.show_pins = False

        # get layer IDs
        hm_layer = 4
        vm_layer = hm_layer + 1
        top_layer = vm_layer

        # create layout masters for subcells we will add later
        inv_master = self.new_template(temp_cls=inverter2, params=dict(params=inv_params))
        tgate_master = self.new_template(temp_cls=tgate, params=dict(params=tgate_params))

        # add subcell instances (inv2 and tgate1 in first row on bottom, inv1 and tgate2 in second row on top)
        inv2_inst = self.add_instance(inv_master, 'inv2', loc=(0, 0), unit_mode=True)
        x1 = inv2_inst.bound_box.right_unit
        tgate1_inst = self.add_instance(tgate_master, 'tgate1', loc=(x1, 0), unit_mode=True)
        x2 = tgate1_inst.bound_box.top_unit
        inv1_inst = self.add_instance(inv_master, 'inv1', loc=(0, x2), unit_mode=True)
        tgate2_inst = self.add_instance(tgate_master, 'tgate2', loc=(x1, x2), unit_mode=True)

        placed_instances = PlacedInstances(template=self, instances=[inv1_inst, inv2_inst, tgate1_inst, tgate2_inst])
        placed_instances.set_size_from_bound_box(top_layer_id=top_layer)

        #
        # add wires, vias, pins
        #
        routing = ManualRouting(
            placed_instances=placed_instances.instances,

            connections_to_tracks=[
                # Net Name              /  Net Horizontal Wires      / Vertical track intersection point
                TrackConnection('ENB',  ['inv1.out', 'inv2.in'],     TrackIntersectionPoint('inv1.out',   UPPER)),
                TrackConnection('EN',   ['tgate1.EN', 'tgate2.ENB'], TrackIntersectionPoint('tgate1.VDD', UPPER)),
                TrackConnection('ENB',  ['tgate1.ENB', 'tgate2.EN'], TrackIntersectionPoint('tgate1.ENB', UPPER)),
                TrackConnection('IN',   ['tgate1.I', 'tgate2.I'],    TrackIntersectionPoint('tgate1.VDD', UPPER, -1)),
                TrackConnection('EN',   ['tgate1.EN', 'inv2.out'],   TrackIntersectionPoint('inv1.VDD',   UPPER)),
                TrackConnection('ENB',  ['tgate2.EN', 'inv1.out'],   TrackIntersectionPoint('inv1.VDD',   UPPER)),
                TrackConnection('VDD',  ['inv1.VDD', 'tgate1.VDD',
                                         'inv2.VDD', 'tgate2.VDD'],  TrackIntersectionPoint('inv1.VDD',   LOWER)),
                TrackConnection('VSS',  ['inv1.VSS', 'tgate1.VSS',
                                         'inv2.VSS', 'tgate2.VSS'],  TrackIntersectionPoint('tgate1.VDD', LOWER)),
                TrackConnection('ENB',  ['tgate2.EN', 'inv1.out'],   TrackIntersectionPoint('inv1.VDD',   UPPER)),
            ],

            net_pins={
                'VDD': ['inv2.VDD', 'tgate2.VDD'],
                'VSS': ['inv2.VSS', 'tgate2.VSS'],
                'IN': ['tgate1.I', 'tgate2.I']
            },

            reexported_ports=[
                # Net name / Instance pin
                ('CNT',     'inv1.in'),
                ('ENB',     'inv1.out'),
                ('EN',      'inv2.out'),
                ('OUTA',    'tgate1.O'),
                ('OUTB',    'tgate2.O'),
            ]
        )

        router = ManualRouter(template=self, routing=routing, show_pins=params.show_pins)
        router.route()

        # compute schematic parameters.
        self._sch_params = dict(
            inv_params=inv_master.sch_params,
            tgate_params=tgate_master.sch_params,
        )

    @property
    def sch_params(self) -> Dict[str, Any]:
        return self._sch_params


class ac3e_analogmux(layout):
    """
    Class to be used as template in higher level layouts
    """
    def __init__(self, temp_db, lib_name, params, used_names, **kwargs):
        super().__init__(temp_db, lib_name, params, used_names, **kwargs)
